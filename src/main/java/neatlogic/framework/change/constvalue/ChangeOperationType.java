package neatlogic.framework.change.constvalue;

import neatlogic.framework.process.operationauth.core.IOperationType;
import neatlogic.framework.process.operationauth.core.OperationAuthHandlerType;
import neatlogic.framework.util.$;

public enum ChangeOperationType implements IOperationType {

	STARTCHANGESTEP("startchangestep", "开始"),
	COMPLETECHANGESTEP("completechangestep", "完成"),
	ABORTCHANGESTEP("abortchangestep", "取消"),
	COMMENTCHANGESTEP("commentchangestep", "回复"),
	EDITCOMMENTCHANGESTEP("editcommentchangestep", "编辑回复"),
	DELETECOMMENTCHANGESTEP("deletecommentchangestep", "删除回复"),
	EDITCHANGESTEP("editchangestep", "编辑"),
	BATCHEDITCHANGESTEPWORKER("batcheditchangestepworker", "批量编辑处理人"),
	STARTCHANGE("startchange", "开始"),
	SUCCEEDCHANGE("succeedchange", "确认成功"),
	FAILEDCHANGE("failedchange", "确认失败"),
	PAUSECHANGE("pausechange", "暂停"),
	RECOVERCHANGE("recoverchange", "恢复"),
	RESTARTCHANGE("restartchange", "重做"),
	EDITCHANGE("editchange", "编辑");
	
	private String value;
	private String text;
	private ChangeOperationType(String value, String text) {
		this.value = value;
		this.text = text;
	}
	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getText() {
		return $.t(text);
	}

	@Override
	public OperationAuthHandlerType getOperationAuthHandlerType() {
		return OperationAuthHandlerType.STEP;
	}

	public static String getText(String value) {
		for(ChangeOperationType t : values()) {
			if(t.getValue().equals(value)) {
				return t.getText();
			}
		}
		return "";
	}
	
	public static ChangeOperationType getChangeOperationType(String value) {
		for(ChangeOperationType t : values()) {
			if(t.getValue().equals(value)) {
				return t;
			}
		}
		return null;
	}
}
