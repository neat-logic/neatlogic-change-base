package neatlogic.framework.change.constvalue;

import neatlogic.framework.process.stepremind.core.IProcessTaskStepRemindType;
import neatlogic.framework.util.$;

public enum ChangeRemindType implements IProcessTaskStepRemindType{
    PAUSECHANGE("pausechange", "变更暂停提醒", "暂停了变更【processTaskStepName】，原因");

    private String value;
    private String text;
    private String title;
    private ChangeRemindType(String value, String text, String title) {
        this.value = value;
        this.text = text;
        this.title = title;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getText() {
        return $.t(text);
    }

    @Override
    public String getTitle() {
        return $.t(title);
    }

}
