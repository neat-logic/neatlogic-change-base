package neatlogic.framework.change.constvalue;

import neatlogic.framework.process.stephandler.core.IProcessStepHandlerType;
import neatlogic.framework.util.$;

public enum ChangeProcessStepHandlerType implements IProcessStepHandlerType {
    CHANGECREATE("changecreate", "process", "term.change.changecreate"),
    CHANGEHANDLE("changehandle", "process", "term.change.changehandle");

    private final String handler;
    private final String name;
    private final String type;

    ChangeProcessStepHandlerType(String handler, String type, String name) {
        this.handler = handler;
        this.name = name;
        this.type = type;
    }

    @Override
    public String getHandler() {
        return handler;
    }

    @Override
    public String getName() {
        return $.t(name);
    }

    @Override
    public String getType() {
        return type;
    }
}
