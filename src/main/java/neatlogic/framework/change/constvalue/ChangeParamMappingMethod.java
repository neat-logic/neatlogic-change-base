package neatlogic.framework.change.constvalue;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import neatlogic.framework.common.constvalue.IEnum;
import neatlogic.framework.util.$;

import java.util.List;

public enum ChangeParamMappingMethod implements IEnum {
    MANUALINPUT("manualinput", "手动输入");
    private String value;
    private String text;
    private ChangeParamMappingMethod(String value, String text) {
        this.value = value;
        this.text = text;
    }
    public String getValue() {
        return value;
    }
    public String getText() {
        return $.t(text);
    }


    @Override
    public List getValueTextList() {
        JSONArray array = new JSONArray();
        for(ChangeParamMappingMethod method : ChangeParamMappingMethod.values()){
            array.add(new JSONObject(){
                {
                    this.put("value",method.getValue());
                    this.put("text",method.getText());
                }
            });
        }
        return array;
    }
}
