package neatlogic.framework.change.constvalue;

import neatlogic.framework.notify.core.INotifyTriggerType;
import neatlogic.framework.util.I18n;

public enum ChangeTriggerType implements INotifyTriggerType {
    STARTCHANGESTEP("startchangestep", new I18n("开始变更步骤"), new I18n("变更步骤处理人开始处理变更步骤时触发通知")),
    COMPLETECHANGESTEP("completechangestep", new I18n("完成变更步骤"), new I18n("变更步骤处理人完成变更步骤时触发通知")),
    ABORTCHANGESTEP("abortchangestep", new I18n("取消变更步骤"), new I18n("变更步骤处理人忽略当前步骤或暂停整个变更时触发通知")),
    COMMENTCHANGESTEP("commentchangestep", new I18n("评论变更步骤"), new I18n("变更步骤处理人评论时触发通知")),
    COMPLETEALLCHANGESTEP("completeallchangestep", new I18n("完成所有变更步骤"), new I18n("所有变更步骤完成时触发通知")),
    STARTCHANGE("startchange", new I18n("开始变更"), new I18n("变更经理开始变更时触发通知")),
    PAUSECHANGE("pausechange", new I18n("暂停变更"), new I18n("变更经理暂停变更或变更处理人暂停整个变更时触发通知")),
    RECOVERCHANGE("recoverchange", new I18n("恢复变更"), new I18n("变更经理恢复变更时触发通知")),
    RESTARTCHANGE("restartchange", new I18n("重新开始变更"), new I18n("变更经理重新开始变更时触发通知"));

    private String trigger;
    private I18n text;
    private I18n description;

    ChangeTriggerType(String trigger, I18n text, I18n _description) {
        this.trigger = trigger;
        this.text = text;
        this.description = _description;
    }

    @Override
    public String getTrigger() {
        return trigger;
    }

    @Override
    public String getText() {
        return text.toString();
    }

    @Override
    public String getDescription() {
        return description.toString();
    }

}
