package neatlogic.framework.change.constvalue;

import neatlogic.framework.process.audithandler.core.IProcessTaskAuditType;
import neatlogic.framework.util.$;

/**
 * 
* @Time:2020年8月11日
* @ClassName: ChangeAuditType 
* @Description: 变更活动类型枚举类
 */
public enum ChangeAuditType implements IProcessTaskAuditType {

	STARTCHANGESTEP("startchangestep", "开始", "开始【${DATA.changeStepName}】"),
	COMPLETECHANGESTEP("completechangestep", "完成", "完成【${DATA.changeStepName}】"),
	ABORTCHANGESTEP("abortchangestep", "取消", "取消【${DATA.changeStepName}】"),
	COMMENTCHANGESTEP("commentchangestep", "回复", "回复【${DATA.changeStepName}】"),
	EDITCOMMENTCHANGESTEP("editcommentchangestep", "编辑回复", "编辑回复【${DATA.changeStepName}】"),
	DELETECOMMENTCHANGESTEP("deletecommentchangestep", "删除回复", "删除回复【${DATA.changeStepName}】"),
	EDITCHANGESTEP("editchangestep", "编辑", "编辑【${DATA.changeStepName}】"),
	BATCHEDITCHANGESTEPWORKER("batcheditchangestepworker", "批量编辑处理人", "批量编辑变更步骤处理人"),
	STARTCHANGE("startchange", "开始", "开始【${DATA.processTaskStepName}】"),
	SUCCEEDCHANGE("succeedchange", "确认成功", "确认成功【${DATA.processTaskStepName}】"),
	FAILEDCHANGE("failedchange", "确认失败", "确认失败【${DATA.processTaskStepName}】"),
	PAUSECHANGE("pausechange", "暂停", "暂停【${DATA.processTaskStepName}】"),
	RECOVERCHANGE("recoverchange", "恢复", "恢复【${DATA.processTaskStepName}】"),
	RESTARTCHANGE("restartchange", "重做", "重新开始【${DATA.processTaskStepName}】"),
	UPDATECHANGE("updatechange", "更新变更", "修改变更信息"),
	//任务
	CREATETASK("createtask", "创建子任务", "创建${DATA.replaceable_task}【${DATA.processTaskStepName}】"),
	EDITTASK("edittask", "编辑子任务", "编辑${DATA.replaceable_task}【${DATA.processTaskStepName}】"),
	ABORTTASK("deletetask", "删除子任务", "删除${DATA.replaceable_task}【${DATA.processTaskStepName}】"),
	COMPLETETASK("completetask", "回复子任务", "完成${DATA.replaceable_task}【${DATA.processTaskStepName}】");
	
	private String value;
	private String text;
	private String description;
	private ChangeAuditType(String value, String text, String description) {
		this.value = value;
		this.text = text;
		this.description = description;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getText() {
		return $.t(text);
	}

	@Override
	public String getDescription() {
		return $.t(description);
	}

	public static ChangeAuditType getChangeAuditType(String value) {
		for(ChangeAuditType t : values()) {
			if(t.getValue().equals(value)) {
				return t;
			}
		}
		return null;
	}

}
