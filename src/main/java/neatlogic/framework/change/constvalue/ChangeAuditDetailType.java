package neatlogic.framework.change.constvalue;

import neatlogic.framework.process.audithandler.core.IProcessTaskAuditDetailType;
import neatlogic.framework.util.$;

/**
 * 
* @Time:2020年8月11日
* @ClassName: ChangeAuditDetailType 
* @Description: 变更活动内容类型枚举类
 */
public enum ChangeAuditDetailType implements IProcessTaskAuditDetailType {

	CHANGEINFO("changeinfo", "变更", "change", "oldChange", 12, false),
	CHANGESTEPINFO("changestepinfo", "变更步骤", "changestep", "oldChangestep", 13, false),
	WORKER("worker", "处理人", "worker", "oldWorker", 14, false),
	TASK("task", "任务", "task", "oldTask", 18, false)
	;
	private String value;
	private String text;
	private String paramName;
	private String oldDataParamName;
	private int sort;
	private boolean needCompression;
	private ChangeAuditDetailType(String _value, String _text, String _paramName, String _oldDataParamName, int _sort, boolean _needCompression) {
		this.value = _value;
		this.text = _text;
		this.paramName = _paramName;
		this.oldDataParamName = _oldDataParamName;
		this.sort = _sort;
		this.needCompression = _needCompression;
	}
	
	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getText() {
		return $.t(text);
	}

	@Override
	public String getParamName() {
		return paramName;
	}

	@Override
	public String getOldDataParamName() {
		return oldDataParamName;
	}

	@Override
	public int getSort() {
		return sort;
	}

	@Override
	public boolean getNeedCompression() {
		return needCompression;
	}

}
