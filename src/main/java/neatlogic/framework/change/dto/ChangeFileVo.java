package neatlogic.framework.change.dto;

public class ChangeFileVo {
	private Long changeId;
	private Long fileId;
	public ChangeFileVo() {
	}
	public ChangeFileVo(Long changeId, Long fileId) {
		this.changeId = changeId;
		this.fileId = fileId;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
}
