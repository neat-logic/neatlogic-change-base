package neatlogic.framework.change.dto;

public class ChangeStepTeamVo {
	private Long changeId;
	private Long changeStepId;
	private String teamUuid;
	private String teamName;
	public ChangeStepTeamVo(){}
	public ChangeStepTeamVo(Long changeId, Long changeStepId, String teamUuid) {
		this.changeId = changeId;
		this.changeStepId = changeStepId;
		this.teamUuid = teamUuid;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public Long getChangeStepId() {
		return changeStepId;
	}
	public void setChangeStepId(Long changeStepId) {
		this.changeStepId = changeStepId;
	}
	public String getTeamUuid() {
		return teamUuid;
	}
	public void setTeamUuid(String teamUuid) {
		this.teamUuid = teamUuid;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}
