package neatlogic.framework.change.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import neatlogic.framework.dto.UserVo;
import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.file.dto.FileVo;
import neatlogic.framework.restful.annotation.EntityField;
import neatlogic.framework.util.SnowflakeUtil;

public class ChangeVo {

	@EntityField(name = "变更id", type = ApiParamType.LONG)
	private Long id;
	@EntityField(name = "计划开始时间", type = ApiParamType.LONG)
	private String planStartTime;
	@EntityField(name = "计划结束时间", type = ApiParamType.LONG)
	private String planEndTime;
	@EntityField(name = "实际开始时间", type = ApiParamType.LONG)
	private Date startTime;
	@EntityField(name = "实际结束时间", type = ApiParamType.LONG)
	private Date endTime;
	@EntityField(name = "开始时间窗口", type = ApiParamType.STRING)
	private String startTimeWindow;
	@EntityField(name = "结束时间窗口", type = ApiParamType.STRING)
	private String endTimeWindow;
	@EntityField(name = "变更代报人", type = ApiParamType.STRING)
	private String reporter;
	@EntityField(name = "是否自动开始", type = ApiParamType.INTEGER)
	private Integer autoStart;
	@EntityField(name = "状态", type = ApiParamType.STRING)
	private String status = "pending";
	@EntityField(name = "状态信息", type = ApiParamType.JSONOBJECT)
	private ChangeStatusVo statusVo;
	@EntityField(name = "步骤列表", type = ApiParamType.JSONARRAY)
	private List<ChangeStepVo> changeStepList = new ArrayList<>();
	@EntityField(name = "变更经理uuid", type = ApiParamType.STRING)
	private String owner;
	@EntityField(name = "变更经理")
	private UserVo ownerVo;
//	@EntityField(name = "变更经理", type = ApiParamType.STRING)
//    private String ownerName;
//	@EntityField(name = "变更经理头像", type = ApiParamType.STRING)
//    private String avatar;
//	@EntityField(name = "变更经理VIP等级", type = ApiParamType.INTEGER)
//	private Integer vipLevel;
	
	@EntityField(name = "计划起止时间", type = ApiParamType.JSONARRAY)
	private List<String> planStartEndTime = new ArrayList<>();
	
	@EntityField(name = "权限操作按钮列表", type = ApiParamType.JSONARRAY)
	private List<ValueTextVo> actionList = new ArrayList<>();
    @EntityField(name = "描述内容", type = ApiParamType.STRING)
    private String content;
    @EntityField(name = "附件列表", type = ApiParamType.JSONARRAY)
    private List<FileVo> fileList = new ArrayList<>();
    @EntityField(name = "附件id列表", type = ApiParamType.JSONARRAY)
    private List<Long> fileIdList;
    
    @EntityField(name = "变更模板id", type = ApiParamType.LONG)
    private Long changeTemplateId;
    @EntityField(name = "变更变量数据", type = ApiParamType.JSONOBJECT)
    private Map<String, Object> paramDataMap = new HashMap<>();
	@EntityField(name = "步骤状态列表", type = ApiParamType.JSONARRAY)
    private List<ChangeStatusVo> changeStatusVoList;
	@JSONField(serialize = false)
	private Boolean isAutoGenerateId = true;
	@JSONField(serialize = false)
	private String configHash;
	
	public synchronized Long getId() {
		if(id == null && isAutoGenerateId) {
			id = SnowflakeUtil.uniqueLong();
		}
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlanStartTime() {
		if(planStartTime == null && CollectionUtils.isNotEmpty(planStartEndTime) && planStartEndTime.size() == 2) {
			planStartTime = planStartEndTime.get(0);
		}
		return planStartTime;
	}
	public void setPlanStartTime(String planStartTime) {
		this.planStartTime = planStartTime;
	}
	public String getPlanEndTime() {
		if(planEndTime == null && CollectionUtils.isNotEmpty(planStartEndTime) && planStartEndTime.size() == 2) {
			planEndTime = planStartEndTime.get(1);
		}
		return planEndTime;
	}
	public void setPlanEndTime(String planEndTime) {
		this.planEndTime = planEndTime;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getStartTimeWindow() {
		return startTimeWindow;
	}
	public void setStartTimeWindow(String startTimeWindow) {
		this.startTimeWindow = startTimeWindow;
	}
	public String getEndTimeWindow() {
		return endTimeWindow;
	}
	public void setEndTimeWindow(String endTimeWindow) {
		this.endTimeWindow = endTimeWindow;
	}
	public String getReporter() {
		return reporter;
	}
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}
	public Integer getAutoStart() {
		return autoStart;
	}
	public void setAutoStart(Integer autoStart) {
		this.autoStart = autoStart;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ChangeStatusVo getStatusVo() {
		if(statusVo == null && StringUtils.isNotBlank(status)) {
			statusVo = new ChangeStatusVo(status);
		}
		return statusVo;
	}
	public void setStatusVo(ChangeStatusVo statusVo) {
		this.statusVo = statusVo;
	}
	public Boolean getIsAutoGenerateId() {
		return isAutoGenerateId;
	}
	public void setIsAutoGenerateId(Boolean isAutoGenerateId) {
		this.isAutoGenerateId = isAutoGenerateId;
	}
	public List<ChangeStepVo> getChangeStepList() {
		return changeStepList;
	}
	public void setChangeStepList(List<ChangeStepVo> changeStepList) {
		this.changeStepList = changeStepList;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public UserVo getOwnerVo() {
		return ownerVo;
	}

	public void setOwnerVo(UserVo ownerVo) {
		this.ownerVo = ownerVo;
	}
	//	public String getOwnerName() {
//        return ownerName;
//    }
//    public void setOwnerName(String ownerName) {
//        this.ownerName = ownerName;
//    }
//    public String getAvatar() {
//        return avatar;
//    }
//    public void setAvatar(String avatar) {
//        this.avatar = avatar;
//    }
//
//	public Integer getVipLevel() {
//		return vipLevel;
//	}
//
//	public void setVipLevel(Integer vipLevel) {
//		this.vipLevel = vipLevel;
//	}

	public List<String> getPlanStartEndTime() {
		if(CollectionUtils.isEmpty(planStartEndTime)) {
			if(planStartTime != null && planEndTime != null) {
				planStartEndTime.add(planStartTime);
				planStartEndTime.add(planEndTime);
			}
		}
		return planStartEndTime;
	}
	public void setPlanStartEndTime(List<String> planStartEndTime) {
		this.planStartEndTime = planStartEndTime;
	}
	public List<ValueTextVo> getActionList() {
		return actionList;
	}
	public void setActionList(List<ValueTextVo> actionList) {
		this.actionList = actionList;
	}
	public String getConfigHash() {
		return configHash;
	}
	public void setConfigHash(String configHash) {
		this.configHash = configHash;
	}
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public List<FileVo> getFileList() {
        return fileList;
    }
    public void setFileList(List<FileVo> fileList) {
        this.fileList = fileList;
    }
    public List<Long> getFileIdList() {
        return fileIdList;
    }
    public void setFileIdList(List<Long> fileIdList) {
        this.fileIdList = fileIdList;
    }
    public Long getChangeTemplateId() {
        return changeTemplateId;
    }
    public void setChangeTemplateId(Long changeTemplateId) {
        this.changeTemplateId = changeTemplateId;
    }
    public Map<String, Object> getParamDataMap() {
        return paramDataMap;
    }
    public void setParamDataMap(Map<String, Object> paramDataMap) {
        this.paramDataMap = paramDataMap;
    }

	public List<ChangeStatusVo> getChangeStatusVoList() {
		return changeStatusVoList;
	}

	public void setChangeStatusVoList(List<ChangeStatusVo> changeStatusVoList) {
		this.changeStatusVoList = changeStatusVoList;
	}
}
