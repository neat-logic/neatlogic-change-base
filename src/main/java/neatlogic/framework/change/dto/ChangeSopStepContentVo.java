package neatlogic.framework.change.dto;

import neatlogic.framework.common.dto.BaseEditorVo;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeSopStepContentVo 
* @Description: 变更sop模板步骤描述类
 */
public class ChangeSopStepContentVo extends BaseEditorVo {
	private Long id;
	private Long changeSopId;
    private String changeSopStepUuid;
	private String contentHash;

	public ChangeSopStepContentVo() {

	}

	public ChangeSopStepContentVo(Long _changeSopId, String _changeSopStepUuid, String _contentHash) {
	    changeSopId = _changeSopId;
        changeSopStepUuid = _changeSopStepUuid;
		contentHash = _contentHash;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }
    public String getChangeSopStepUuid() {
        return changeSopStepUuid;
    }
    public void setChangeSopStepUuid(String changeSopStepUuid) {
        this.changeSopStepUuid = changeSopStepUuid;
    }

	public String getContentHash() {
		return contentHash;
	}

	public void setContentHash(String contentHash) {
		this.contentHash = contentHash;
	}

}
