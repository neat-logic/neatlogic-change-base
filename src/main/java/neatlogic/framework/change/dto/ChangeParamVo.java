package neatlogic.framework.change.dto;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.ParamType;
import neatlogic.framework.restful.annotation.EntityField;
import neatlogic.framework.util.SnowflakeUtil;

/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeParamVo 
* @Description: 变更变量类
 */
public class ChangeParamVo {
    @EntityField(name = "变量id", type = ApiParamType.LONG)
    private Long id;
    @EntityField(name = "变量名称", type = ApiParamType.STRING)
    private String name;
    @EntityField(name = "映射方式", type = ApiParamType.STRING)
    private String mappingMethod;
    @EntityField(name = "对应的freemarker变量模板", type = ApiParamType.STRING)
    private String freemarkerTemplate;

    @JSONField(serialize = false)
    private Boolean isAutoGenerateId = true;
    
    public synchronized Long getId() {
        if(id == null && isAutoGenerateId) {
            id = SnowflakeUtil.uniqueLong();
        }
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getMappingMethod() {
        return mappingMethod;
    }
    public void setMappingMethod(String mappingMethod) {
        this.mappingMethod = mappingMethod;
    }
    public String getFreemarkerTemplate() {
        if(StringUtils.isBlank(freemarkerTemplate) && StringUtils.isNotBlank(name)) {
            freemarkerTemplate = ParamType.STRING.getFreemarkerTemplate(name);
        }
        return freemarkerTemplate;
    }
    public void setFreemarkerTemplate(String freemarkerTemplate) {
        this.freemarkerTemplate = freemarkerTemplate;
    }
    public Boolean getIsAutoGenerateId() {
        return isAutoGenerateId;
    }
    public void setIsAutoGenerateId(Boolean isAutoGenerateId) {
        this.isAutoGenerateId = isAutoGenerateId;
    }
}
