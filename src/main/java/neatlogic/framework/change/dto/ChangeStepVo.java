package neatlogic.framework.change.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Objects;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.dto.ValueTextVo;
import neatlogic.framework.file.dto.FileVo;
import neatlogic.framework.restful.annotation.EntityField;
import neatlogic.framework.util.SnowflakeUtil;
import neatlogic.framework.util.TimeUtil;
import neatlogic.framework.change.constvalue.ChangeStatus;

public class ChangeStepVo {
	
	@EntityField(name = "变更步骤id", type = ApiParamType.LONG)
	private Long id;
	@EntityField(name = "变更id", type = ApiParamType.LONG)
	private Long changeId;
//	@EntityField(name = "计划开始时间", type = ApiParamType.LONG)
//	private Date planStartTime;
//	@EntityField(name = "计划结束时间", type = ApiParamType.LONG)
//	private Date planEndTime;
	@EntityField(name = "计划开始日期", type = ApiParamType.STRING)
	private String planStartDate;
	@EntityField(name = "实际开始时间", type = ApiParamType.LONG)
	private Date startTime;
	@EntityField(name = "实际结束时间", type = ApiParamType.LONG)
	private Date endTime;
	@EntityField(name = "取消时间", type = ApiParamType.LONG)
	private Date abortTime;
	@EntityField(name = "开始时间窗口", type = ApiParamType.STRING)
	private String startTimeWindow;
	@EntityField(name = "结束时间窗口", type = ApiParamType.STRING)
	private String endTimeWindow;
	private Integer isActive = 0;
	@EntityField(name = "状态", type = ApiParamType.STRING)
	private String status = "pending";
	@EntityField(name = "状态信息", type = ApiParamType.JSONOBJECT)
	private ChangeStatusVo statusVo;
	@EntityField(name = "编码", type = ApiParamType.STRING)
	private String code;
	@EntityField(name = "权限操作按钮列表", type = ApiParamType.JSONARRAY)
	private List<ValueTextVo> actionList = new ArrayList<>();

	@EntityField(name = "步骤uuid", type = ApiParamType.STRING)
	private String uuid;
	@EntityField(name = "处理人或组")
	private Object workerVo;
	@EntityField(name = "处理人或组uuid", type = ApiParamType.STRING)
	private String worker;
//	@EntityField(name = "处理人或组", type = ApiParamType.STRING)
//	private String workerName;
//	@EntityField(name = "处理人头像", type = ApiParamType.STRING)
//	private String workerAvatar;
//	@EntityField(name = "处理人VIP等级", type = ApiParamType.INTEGER)
//	private Integer workerVipLevel;
	@EntityField(name = "步骤名称", type = ApiParamType.STRING)
	private String name;
	@EntityField(name = "描述内容", type = ApiParamType.STRING)
	private String content;
	@EntityField(name = "附件列表", type = ApiParamType.JSONARRAY)
	private List<FileVo> fileList = new ArrayList<>();
	@EntityField(name = "附件id列表", type = ApiParamType.JSONARRAY)
	private List<Long> fileIdList;
	@EntityField(name = "回复列表", type = ApiParamType.JSONARRAY)
	private List<ChangeStepCommentVo> commentList = new ArrayList<>();
	@EntityField(name = "是否在时间窗口内", type = ApiParamType.INTEGER)
	private Integer isInWindow;
	@EntityField(name = "是否在计划开始日期之后", type = ApiParamType.INTEGER)
	private Integer isAfterStartDate;
	
	@EntityField(name = "变更状态", type = ApiParamType.STRING)
    private String changeStatus;

	@JSONField(serialize = false)
	private boolean isAutoGenerateId = true;
	
	public ChangeStepVo() {
    }
	public ChangeStepVo(ChangeSopStepVo changeSopStepVo) {
	    this.uuid = changeSopStepVo.getUuid();
	    this.name = changeSopStepVo.getName();
	    this.planStartDate = changeSopStepVo.getPlanStartDate();
	    this.startTimeWindow = changeSopStepVo.getStartTimeWindow();
	    this.endTimeWindow = changeSopStepVo.getEndTimeWindow();
	    this.code = changeSopStepVo.getCode();
	    this.workerVo = changeSopStepVo.getWorkerVo();
	    this.worker = changeSopStepVo.getWorker();
//	    this.worker = changeSopStepVo.getWorker();
//	    this.workerName = changeSopStepVo.getWorkerName();
//	    this.workerAvatar = changeSopStepVo.getWorkerAvatar();
//	    this.workerVipLevel = changeSopStepVo.getWorkerVipLevel();
	    this.content = changeSopStepVo.getContent();
	    this.fileIdList = changeSopStepVo.getFileIdList();
	    this.fileList = changeSopStepVo.getFileList();
    }
    public synchronized Long getId() {
		if(id == null && isAutoGenerateId) {
			id = SnowflakeUtil.uniqueLong();
		}
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public String getPlanStartDate() {
		return planStartDate;
	}
	public void setPlanStartDate(String planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getAbortTime() {
		return abortTime;
	}
	public void setAbortTime(Date abortTime) {
		this.abortTime = abortTime;
	}
	public String getStartTimeWindow() {
		return startTimeWindow;
	}
	public void setStartTimeWindow(String startTimeWindow) {
		this.startTimeWindow = startTimeWindow;
	}
	public String getEndTimeWindow() {
		return endTimeWindow;
	}
	public void setEndTimeWindow(String endTimeWindow) {
		this.endTimeWindow = endTimeWindow;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ChangeStatusVo getStatusVo() {
		if(statusVo == null && StringUtils.isNotBlank(status)) {
			statusVo = new ChangeStatusVo(status);
		}
		return statusVo;
	}
	public void setStatusVo(ChangeStatusVo statusVo) {
		this.statusVo = statusVo;
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<ValueTextVo> getActionList() {
		return actionList;
	}
	public void setActionList(List<ValueTextVo> actionList) {
		this.actionList = actionList;
	}
	
	public boolean isAutoGenerateId() {
		return isAutoGenerateId;
	}
	public void setAutoGenerateId(boolean isAutoGenerateId) {
		this.isAutoGenerateId = isAutoGenerateId;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Object getWorkerVo() {
		return workerVo;
	}

	public void setWorkerVo(Object workerVo) {
		this.workerVo = workerVo;
	}

	public String getWorker() {
		return worker;
	}
	public void setWorker(String worker) {
		this.worker = worker;
	}
//	public String getWorkerName() {
//		return workerName;
//	}
//	public void setWorkerName(String workerName) {
//		this.workerName = workerName;
//	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<FileVo> getFileList() {
		return fileList;
	}
	public void setFileList(List<FileVo> fileList) {
		this.fileList = fileList;
	}
	public List<Long> getFileIdList() {
		return fileIdList;
	}
	public void setFileIdList(List<Long> fileIdList) {
		this.fileIdList = fileIdList;
	}
	public List<ChangeStepCommentVo> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<ChangeStepCommentVo> commentList) {
		this.commentList = commentList;
	}
	public Integer getIsInWindow() {
		if(isInWindow == null) {
		    isInWindow = 1;
		    if(ChangeStatus.RUNNING.getValue().equals(changeStatus) && Objects.equal(isActive, 1)) {
		        if(ChangeStatus.PENDING.getValue().equals(status) || ChangeStatus.RUNNING.getValue().equals(status)) {
		            if(TimeUtil.isInTimeWindow(startTimeWindow, endTimeWindow) != 0) {
	                    isInWindow = 0;
	                }
		        }
		    }
		}
		return isInWindow;
	}
	public void setIsInWindow(Integer isInWindow) {
		this.isInWindow = isInWindow;
	}
	public Integer getIsAfterStartDate() {
		if(isAfterStartDate == null) {
		    isAfterStartDate = 1;
		    if(ChangeStatus.RUNNING.getValue().equals(changeStatus) && Objects.equal(isActive, 1)) {
		        if(ChangeStatus.PENDING.getValue().equals(status) || ChangeStatus.RUNNING.getValue().equals(status)) {
		            if(StringUtils.isNotBlank(planStartDate)) {
		                try {
	                        if(new SimpleDateFormat(TimeUtil.YYYY_MM_DD).parse(planStartDate).after(new Date())) {
	                            isAfterStartDate = 0;
	                        }
		                }catch(ParseException e) {
		                    e.printStackTrace();
		                }
	                }
                }
		    }			
		}
		return isAfterStartDate;
	}
	public void setIsAfterStartDate(Integer isAfterStartDate) {
		this.isAfterStartDate = isAfterStartDate;
	}
    public String getChangeStatus() {
        return changeStatus;
    }
    public void setChangeStatus(String changeStatus) {
        this.changeStatus = changeStatus;
    }
//	public String getWorkerAvatar() {
//		return workerAvatar;
//	}
//
//	public void setWorkerAvatar(String workerAvatar) {
//		this.workerAvatar = workerAvatar;
//	}
//
//	public Integer getWorkerVipLevel() {
//		return workerVipLevel;
//	}
//
//	public void setWorkerVipLevel(Integer workerVipLevel) {
//		this.workerVipLevel = workerVipLevel;
//	}
}
