package neatlogic.framework.change.dto;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.common.constvalue.GroupSearch;
import neatlogic.framework.common.dto.BaseEditorVo;
import neatlogic.framework.restful.annotation.EntityField;
import neatlogic.framework.util.SnowflakeUtil;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeTemplateVo 
* @Description: 变更模板类
 */
public class ChangeTemplateVo extends BaseEditorVo {

    @EntityField(name = "变更模板id", type = ApiParamType.LONG)
    private Long id;
    @EntityField(name = "名称", type = ApiParamType.STRING)
    private String name;
    @EntityField(name = "是否激活", type = ApiParamType.INTEGER)
    private Integer isActive;
    @EntityField(name = "分组uuid，格式：team#uuid", type = ApiParamType.STRING)
    private String authority;
    @EntityField(name = "类型", type = ApiParamType.STRING)
    private String type;
    @EntityField(name = "变更sop模板id列表", type = ApiParamType.JSONARRAY)
    private List<Long> changeSopIdList = new ArrayList<>();
    @EntityField(name = "变更sop模板列表", type = ApiParamType.JSONARRAY)
    private List<ChangeSopVo> changeSopList = new ArrayList<>();
    @JSONField(serialize = false)
    private Boolean isAutoGenerateId = true;
    @JSONField(serialize = false)
    private String teamUuid;
    public synchronized Long getId() {
        if(id == null && isAutoGenerateId) {
            id = SnowflakeUtil.uniqueLong();
        }
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getIsActive() {
        return isActive;
    }
    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
    public String getTeamUuid() {
        if(StringUtils.isBlank(teamUuid) && StringUtils.isNotBlank(authority)) {
            teamUuid = authority.substring(GroupSearch.TEAM.getValuePlugin().length());
        }
        return teamUuid;
    }
    public void setTeamUuid(String teamUuid) {
        this.teamUuid = teamUuid;
    }
    public String getAuthority() {
        if(StringUtils.isBlank(authority) && StringUtils.isNotBlank(teamUuid)) {
            authority = GroupSearch.TEAM.getValuePlugin() + teamUuid;
        }
        return authority;
    }
    public void setAuthority(String authority) {
        this.authority = authority;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Boolean getIsAutoGenerateId() {
        return isAutoGenerateId;
    }
    public void setIsAutoGenerateId(Boolean isAutoGenerateId) {
        this.isAutoGenerateId = isAutoGenerateId;
    }
    public List<Long> getChangeSopIdList() {
        return changeSopIdList;
    }
    public void setChangeSopIdList(List<Long> changeSopIdList) {
        this.changeSopIdList = changeSopIdList;
    }
    public List<ChangeSopVo> getChangeSopList() {
        return changeSopList;
    }
    public void setChangeSopList(List<ChangeSopVo> changeSopList) {
        this.changeSopList = changeSopList;
    }

}
