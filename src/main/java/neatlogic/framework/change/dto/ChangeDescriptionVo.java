package neatlogic.framework.change.dto;

import java.util.Date;

import neatlogic.framework.common.dto.BaseEditorVo;
import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.asynchronization.threadlocal.UserContext;

public class ChangeDescriptionVo extends BaseEditorVo {
	private Long changeId;
	private String contentHash;

	public ChangeDescriptionVo() {

	}

	public ChangeDescriptionVo(Long _changeId, String _contentHash) {
		changeId = _changeId;
		contentHash = _contentHash;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public String getContentHash() {
		return contentHash;
	}

	public void setContentHash(String contentHash) {
		this.contentHash = contentHash;
	}

}
