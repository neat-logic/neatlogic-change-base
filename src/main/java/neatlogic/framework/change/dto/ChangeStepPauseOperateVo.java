package neatlogic.framework.change.dto;

public class ChangeStepPauseOperateVo {

    private Long changeId;
    private Long changeStepId;
    private String fcu;
    public ChangeStepPauseOperateVo() {}
    public ChangeStepPauseOperateVo(Long changeId, Long changeStepId, String fcu) {
        this.changeId = changeId;
        this.changeStepId = changeStepId;
        this.fcu = fcu;
    }
    public Long getChangeId() {
        return changeId;
    }
    public void setChangeId(Long changeId) {
        this.changeId = changeId;
    }
    public Long getChangeStepId() {
        return changeStepId;
    }
    public void setChangeStepId(Long changeStepId) {
        this.changeStepId = changeStepId;
    }
    public String getFcu() {
        return fcu;
    }
    public void setFcu(String fcu) {
        this.fcu = fcu;
    }
}
