package neatlogic.framework.change.dto;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeSopStepTeamVo 
* @Description: 变更sop模板步骤处理组类
 */
public class ChangeSopStepTeamVo {
    private Long changeSopId;
    private String changeSopStepUuid;
	private String teamUuid;
	private String teamName;
	public ChangeSopStepTeamVo(){}
	public ChangeSopStepTeamVo(Long changeSopId, String changeSopStepUuid, String teamUuid) {
	    this.changeSopId = changeSopId;
        this.changeSopStepUuid = changeSopStepUuid;
		this.teamUuid = teamUuid;
	}
	public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }
    public String getChangeSopStepUuid() {
        return changeSopStepUuid;
    }
    public void setChangeSopStepUuid(String changeSopStepUuid) {
        this.changeSopStepUuid = changeSopStepUuid;
    }
	public String getTeamUuid() {
		return teamUuid;
	}
	public void setTeamUuid(String teamUuid) {
		this.teamUuid = teamUuid;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
}
