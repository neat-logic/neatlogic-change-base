package neatlogic.framework.change.dto;

import neatlogic.framework.dto.UserVo;

/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeSopStepUserVo 
* @Description: 变更sop模板步骤处理人类
 */
public class ChangeSopStepUserVo {
	private Long changeSopId;
	private String changeSopStepUuid;
	private UserVo userVo;
	public ChangeSopStepUserVo() {}
	public ChangeSopStepUserVo(Long changeSopId, String changeSopStepUuid, String userUuid) {
		this.changeSopId = changeSopId;
		this.changeSopStepUuid = changeSopStepUuid;
		this.userVo = new UserVo(userUuid);
	}
	
	public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }
    public String getChangeSopStepUuid() {
        return changeSopStepUuid;
    }
    public void setChangeSopStepUuid(String changeSopStepUuid) {
        this.changeSopStepUuid = changeSopStepUuid;
    }

    public UserVo getUserVo() {
        return userVo;
    }

    public void setUserVo(UserVo userVo) {
        this.userVo = userVo;
    }

}
