package neatlogic.framework.change.dto;

import neatlogic.framework.dto.UserVo;

public class ChangeStepUserVo {
	private Long changeId;
	private Long changeStepId;
	private UserVo userVo;
//	private String userUuid;
//	private String userName;
//	private String userInfo;
//	private String avatar;
//	private Integer vipLevel;
	public ChangeStepUserVo() {}
	public ChangeStepUserVo(Long changeId, Long changeStepId, String userUuid) {
		this.changeId = changeId;
		this.changeStepId = changeStepId;
		this.userVo = new UserVo(userUuid);
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public Long getChangeStepId() {
		return changeStepId;
	}
	public void setChangeStepId(Long changeStepId) {
		this.changeStepId = changeStepId;
	}

	public UserVo getUserVo() {
		return userVo;
	}

	public void setUserVo(UserVo userVo) {
		this.userVo = userVo;
	}
	//	public String getUserUuid() {
//		return userUuid;
//	}
//	public void setUserUuid(String userUuid) {
//		this.userUuid = userUuid;
//	}
//	public String getUserName() {
//		return userName;
//	}
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	public String getUserInfo() {
//		return userInfo;
//	}
//
//	public void setUserInfo(String userInfo) {
//		this.userInfo = userInfo;
//	}
//
//	public String getAvatar() {
//		if (StringUtils.isBlank(avatar) && StringUtils.isNotBlank(userInfo)) {
//			JSONObject jsonObject = JSONObject.parseObject(userInfo);
//			avatar = jsonObject.getString("avatar");
//		}
//		return avatar;
//	}
//
//	public void setAvatar(String avatar) {
//		this.avatar = avatar;
//	}
//
//	public Integer getVipLevel() {
//		return vipLevel;
//	}
//
//	public void setVipLevel(Integer vipLevel) {
//		this.vipLevel = vipLevel;
//	}
}
