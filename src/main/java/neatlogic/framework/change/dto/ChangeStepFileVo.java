package neatlogic.framework.change.dto;

public class ChangeStepFileVo {
	private Long changeId;
	private Long changeStepId;
	private Long fileId;
	public ChangeStepFileVo() {
	}
	public ChangeStepFileVo(Long changeId, Long changeStepId, Long fileId) {
		this.changeId = changeId;
		this.changeStepId = changeStepId;
		this.fileId = fileId;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public Long getChangeStepId() {
		return changeStepId;
	}
	public void setChangeStepId(Long changeStepId) {
		this.changeStepId = changeStepId;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
}
