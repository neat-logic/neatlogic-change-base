package neatlogic.framework.change.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.common.constvalue.ApiParamType;
import neatlogic.framework.file.dto.FileVo;
import neatlogic.framework.restful.annotation.EntityField;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeSopStepVo 
* @Description: 变更sop模板步骤信息类
 */
public class ChangeSopStepVo {
    
    @EntityField(name = "变更SOP模板步骤uuid", type = ApiParamType.LONG)
    private String uuid;
    @EntityField(name = "变更SOP模板id", type = ApiParamType.LONG)
    private Long changeSopId;
    @EntityField(name = "变更SOP模板名称", type = ApiParamType.STRING)
    private String changeSopName;
    @EntityField(name = "名称", type = ApiParamType.STRING)
    private String name;
    @EntityField(name = "计划开始日期", type = ApiParamType.STRING)
    private String planStartDate;
    @EntityField(name = "开始时间窗口", type = ApiParamType.STRING)
    private String startTimeWindow;
    @EntityField(name = "结束时间窗口", type = ApiParamType.STRING)
    private String endTimeWindow;
    @EntityField(name = "编码", type = ApiParamType.STRING)
    private String code;
    @EntityField(name = "处理人")
    private Object workerVo;
    @EntityField(name = "处理人或组uuid", type = ApiParamType.STRING)
    private String worker;
//    @EntityField(name = "处理人或组", type = ApiParamType.STRING)
//    private String workerName;
//    @EntityField(name = "处理人头像", type = ApiParamType.STRING)
//    private String workerAvatar;
//    @EntityField(name = "处理人VIP等级", type = ApiParamType.INTEGER)
//    private Integer workerVipLevel;
    @EntityField(name = "描述内容", type = ApiParamType.STRING)
    private String content;
    @EntityField(name = "附件列表", type = ApiParamType.JSONARRAY)
    private List<FileVo> fileList = new ArrayList<>();
    @EntityField(name = "附件id列表", type = ApiParamType.JSONARRAY)
    private List<Long> fileIdList;
    
    public synchronized String getUuid() {
        if (StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString().replace("-", "");
        }
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }

    public String getChangeSopName() {
        return changeSopName;
    }

    public void setChangeSopName(String changeSopName) {
        this.changeSopName = changeSopName;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPlanStartDate() {
        return planStartDate;
    }
    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }
    public String getStartTimeWindow() {
        return startTimeWindow;
    }
    public void setStartTimeWindow(String startTimeWindow) {
        this.startTimeWindow = startTimeWindow;
    }
    public String getEndTimeWindow() {
        return endTimeWindow;
    }
    public void setEndTimeWindow(String endTimeWindow) {
        this.endTimeWindow = endTimeWindow;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public Object getWorkerVo() {
        return workerVo;
    }

    public void setWorkerVo(Object workerVo) {
        this.workerVo = workerVo;
    }

    public String getWorker() {
        return worker;
    }
    public void setWorker(String worker) {
        this.worker = worker;
    }
//    public String getWorkerName() {
//        return workerName;
//    }
//    public void setWorkerName(String workerName) {
//        this.workerName = workerName;
//    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public List<FileVo> getFileList() {
        return fileList;
    }
    public void setFileList(List<FileVo> fileList) {
        this.fileList = fileList;
    }
    public List<Long> getFileIdList() {
        return fileIdList;
    }
    public void setFileIdList(List<Long> fileIdList) {
        this.fileIdList = fileIdList;
    }

//    public String getWorkerAvatar() {
//        return workerAvatar;
//    }
//
//    public void setWorkerAvatar(String workerAvatar) {
//        this.workerAvatar = workerAvatar;
//    }
//
//    public Integer getWorkerVipLevel() {
//        return workerVipLevel;
//    }
//
//    public void setWorkerVipLevel(Integer workerVipLevel) {
//        this.workerVipLevel = workerVipLevel;
//    }
}
