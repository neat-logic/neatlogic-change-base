package neatlogic.framework.change.dto;

import java.util.Date;

public class ChangeAutoStartVo {

	private Long changeId;
	private Date targetTime;
	public ChangeAutoStartVo(Long changeId, Date planStartTime) {
		this.changeId = changeId;
		this.targetTime = planStartTime;
	}
	public Long getChangeId() {
		return changeId;
	}
	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}
	public Date getTargetTime() {
		return targetTime;
	}
	public void setTargetTime(Date targetTime) {
		this.targetTime = targetTime;
	}
}
