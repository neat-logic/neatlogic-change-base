package neatlogic.framework.change.dto;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.DigestUtils;

public class ChangeConfigVo {
	private String hash;
	private String config;

	public ChangeConfigVo() {

	}

	public ChangeConfigVo(String config) {
		this.config = config;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getHash() {
		if (StringUtils.isBlank(hash) && StringUtils.isNotBlank(config)) {
			hash = DigestUtils.md5DigestAsHex(config.getBytes());
		}
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
