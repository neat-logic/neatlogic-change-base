package neatlogic.framework.change.dto;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeTemplateSopVo 
* @Description: 变更模板与sop模板关联类
 */
public class ChangeTemplateSopVo {

    private Long changeTemplateId;
    private Long changeSopId;
    private Integer sort;
    public ChangeTemplateSopVo() {}
    public ChangeTemplateSopVo(Long changeTemplateId, Long changeSopId, Integer sort) {
        this.changeTemplateId = changeTemplateId;
        this.changeSopId = changeSopId;
        this.sort = sort;
    }
    public Long getChangeTemplateId() {
        return changeTemplateId;
    }
    public void setChangeTemplateId(Long changeTemplateId) {
        this.changeTemplateId = changeTemplateId;
    }
    public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }
    public Integer getSort() {
        return sort;
    }
    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
