package neatlogic.framework.change.dto;

import java.util.Date;

import neatlogic.framework.common.dto.BaseEditorVo;
import org.apache.commons.lang3.StringUtils;

import neatlogic.framework.asynchronization.threadlocal.UserContext;

public class ChangeStepContentVo extends BaseEditorVo {
	private Long id;
	private Long changeId;
	private Long changeStepId;
	private String contentHash;

	public ChangeStepContentVo() {

	}

	public ChangeStepContentVo(Long _changeId, Long _changeStepId, String _contentHash) {
		changeId = _changeId;
		changeStepId = _changeStepId;
		contentHash = _contentHash;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getChangeId() {
		return changeId;
	}

	public void setChangeId(Long changeId) {
		this.changeId = changeId;
	}

	public Long getChangeStepId() {
		return changeStepId;
	}

	public void setChangeStepId(Long changeStepId) {
		this.changeStepId = changeStepId;
	}

	public String getContentHash() {
		return contentHash;
	}

	public void setContentHash(String contentHash) {
		this.contentHash = contentHash;
	}

}
