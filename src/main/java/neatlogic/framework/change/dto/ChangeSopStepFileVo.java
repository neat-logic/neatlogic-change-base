package neatlogic.framework.change.dto;
/**
 * 
* @Author:linbq
* @Time:2020年8月18日
* @ClassName: ChangeSopStepFileVo 
* @Description: 变更sop模板步骤附件列表类
 */
public class ChangeSopStepFileVo {
    private Long changeSopId;
    private String changeSopStepUuid;
	private Long fileId;
	public ChangeSopStepFileVo() {
	}
	public ChangeSopStepFileVo(Long changeSopId, String changeSopStepUuid, Long fileId) {
	    this.changeSopId = changeSopId;
        this.changeSopStepUuid = changeSopStepUuid;
		this.fileId = fileId;
	}
	public Long getChangeSopId() {
        return changeSopId;
    }
    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }
    public String getChangeSopStepUuid() {
        return changeSopStepUuid;
    }
    public void setChangeSopStepUuid(String changeSopStepUuid) {
        this.changeSopStepUuid = changeSopStepUuid;
    }
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
}
