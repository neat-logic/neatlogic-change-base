package neatlogic.framework.change.dto;

public class ChangeSopStepParamVo {

    private Long changeSopId;
    private String changeSopStepUuid;
    private Long changeParamId;
    public ChangeSopStepParamVo(Long changeSopId, String changeSopStepUuid, Long changeParamId) {
        this.changeSopId = changeSopId;
        this.changeSopStepUuid = changeSopStepUuid;
        this.changeParamId = changeParamId;
    }
    
    public Long getChangeSopId() {
        return changeSopId;
    }

    public void setChangeSopId(Long changeSopId) {
        this.changeSopId = changeSopId;
    }

    public String getChangeSopStepUuid() {
        return changeSopStepUuid;
    }

    public void setChangeSopStepUuid(String changeSopStepUuid) {
        this.changeSopStepUuid = changeSopStepUuid;
    }

    public Long getChangeParamId() {
        return changeParamId;
    }
    public void setChangeParamId(Long changeParamId) {
        this.changeParamId = changeParamId;
    }
}
