package neatlogic.framework.change.dto;

import neatlogic.framework.change.constvalue.ChangeStatus;

public class ChangeStatusVo {

	private String status;
	private String text;
	private String color;
	public ChangeStatusVo() {
	}
	public ChangeStatusVo(String status) {
		this.status = status;
		this.text = ChangeStatus.getText(status);
		this.color = ChangeStatus.getColor(status);
	}
	public ChangeStatusVo(String status, String text) {
		this.status = status;
		this.text = text;
		this.color = ChangeStatus.getColor(status);
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
