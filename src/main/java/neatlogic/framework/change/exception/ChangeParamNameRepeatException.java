package neatlogic.framework.change.exception;

import neatlogic.framework.exception.core.ApiRuntimeException;

public class ChangeParamNameRepeatException extends ApiRuntimeException {

    private static final long serialVersionUID = -4364605666201432070L;

    public ChangeParamNameRepeatException(String name) {
        super("变量名称：“{0}”已存在", name);
    }
}
