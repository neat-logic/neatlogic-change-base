package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeSopNotFoundException extends ChangeRuntimeException {

    private static final long serialVersionUID = 3340691856447248353L;

    public ChangeSopNotFoundException(Long id) {
		super("变更sop模板：“{0}”不存在", id);
	}
}
