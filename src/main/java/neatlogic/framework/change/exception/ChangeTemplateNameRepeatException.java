package neatlogic.framework.change.exception;

import neatlogic.framework.exception.core.ApiRuntimeException;

public class ChangeTemplateNameRepeatException extends ApiRuntimeException {

    private static final long serialVersionUID = 8947815631898404891L;

    public ChangeTemplateNameRepeatException(String name) {
        super("变更模板：“{0}”已存在", name);
    }
}
