package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

/**
 * @Title: ChangeSopIsReferencedException
 * @Package neatlogic.module.change.exception
 * @Description: 变更sop模板被引用后，不能进行禁用或删除操作
 * @Author: linbq
 * @Date: 2021/3/3 10:40

 **/
public class ChangeSopIsReferencedException extends ChangeRuntimeException {
    private static final long serialVersionUID = 3340691856447348353L;
    public ChangeSopIsReferencedException(String name){
        super("变更sop模板：“{0}”已被引用", name);
    }
}
