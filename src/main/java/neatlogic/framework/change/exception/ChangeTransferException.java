package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeTransferException extends ChangeRuntimeException {

	private static final long serialVersionUID = 3306897712474756348L;

	public ChangeTransferException() {
		super("变更处理节点只能转交到一个处理人");
	}
}
