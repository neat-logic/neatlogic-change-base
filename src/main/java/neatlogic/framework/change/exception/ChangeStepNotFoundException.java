package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeStepNotFoundException extends ChangeRuntimeException {

	private static final long serialVersionUID = 7022225468225130634L;

	public ChangeStepNotFoundException(Long changeStepId) {
		super("变更步骤：“{0}”不存在", changeStepId);
	}
}
