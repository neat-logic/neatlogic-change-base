package neatlogic.framework.change.exception;

import neatlogic.framework.exception.core.NotFoundEditTargetException;

public class ChangeTemplateNotFoundEditTargetException extends NotFoundEditTargetException {

    private static final long serialVersionUID = 7163068013601070554L;

    public ChangeTemplateNotFoundEditTargetException(Long id) {
		super("nfce.changetemplatenotfoundedittargetexception.changetemplatenotfoundedittargetexception", id);
	}
}
