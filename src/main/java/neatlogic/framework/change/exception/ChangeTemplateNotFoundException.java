package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeTemplateNotFoundException extends ChangeRuntimeException {

    private static final long serialVersionUID = 7163068013601070553L;

    public ChangeTemplateNotFoundException(Long id) {
		super("变更模板：“{0}”不存在", id);
	}
}
