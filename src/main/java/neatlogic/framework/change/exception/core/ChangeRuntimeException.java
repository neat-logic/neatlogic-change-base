package neatlogic.framework.change.exception.core;

import neatlogic.framework.exception.core.ApiRuntimeException;

public class ChangeRuntimeException extends ApiRuntimeException {

	private static final long serialVersionUID = 7231160206967649801L;

	public ChangeRuntimeException(String msg) {
		super(msg);
	}

	public ChangeRuntimeException(String key, Object ... values) {
		super(key, values);
	}

}
