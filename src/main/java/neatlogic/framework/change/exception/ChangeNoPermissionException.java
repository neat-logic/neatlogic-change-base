package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeNoPermissionException extends ChangeRuntimeException {

	private static final long serialVersionUID = 2708773883017675547L;

	public ChangeNoPermissionException(String action) {
		super("您没有执行“{0}”操作权限", action);
	}
}
