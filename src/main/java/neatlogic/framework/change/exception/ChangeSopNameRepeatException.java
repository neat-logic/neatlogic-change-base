package neatlogic.framework.change.exception;

import neatlogic.framework.exception.core.ApiRuntimeException;

public class ChangeSopNameRepeatException extends ApiRuntimeException {

    private static final long serialVersionUID = 2662739349155307237L;

    public ChangeSopNameRepeatException(String name) {
        super("变更sop模板：“{0}”已存在", name);
    }
}
