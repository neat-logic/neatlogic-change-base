package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeStepCommentNotFoundException extends ChangeRuntimeException {

	private static final long serialVersionUID = 5080677417225747848L;

	public ChangeStepCommentNotFoundException(Long id) {
		super("变更步骤回复：“{0}”不存在", id);
	}
}
