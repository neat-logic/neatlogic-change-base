package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeNotFoundException extends ChangeRuntimeException {

	private static final long serialVersionUID = -642037748484031163L;

	public ChangeNotFoundException(Long changeId) {
		super("变更：“{0}”不存在", changeId);
	}
}
