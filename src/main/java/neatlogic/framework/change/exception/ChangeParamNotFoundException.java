package neatlogic.framework.change.exception;

import neatlogic.framework.change.exception.core.ChangeRuntimeException;

public class ChangeParamNotFoundException extends ChangeRuntimeException {

    private static final long serialVersionUID = -7299198192769599092L;

    public ChangeParamNotFoundException(String id) {
		super("变量：“{0}”不存在", id);
	}
}
